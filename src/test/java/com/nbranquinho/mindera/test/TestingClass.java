
package com.nbranquinho.mindera.test;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;

public class TestingClass {
	public WebDriver driver;

	@BeforeClass
	public void BeforeClass() throws MalformedURLException {
		DesiredCapabilities dcap = DesiredCapabilities.chrome();
		String driverPath = System.getProperty("user.dir") + "/exe/chromedriver";
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new RemoteWebDriver(new URL("http://localhost:3000"), dcap);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

	@Test
	public void ChangeListOrder() {
		driver.manage().window().maximize();
	    	    
	    //open the URL
	    driver.get("http://localhost:3000");
	    
	    //get the list element (ul) inside the main DIV (id = app)
    	WebElement listDiv = driver.findElement(By.xpath("//*[@id='app']/ul"));
    	
    	List<WebElement> itensList = listDiv.findElements(By.xpath("//li"));
    	
    	//Using Action class for drag and drop.		
        Actions act=new Actions(driver);	
        
    	for (int i = itensList.size()-1; i >= 0 ; i--) {
    		 
			WebElement webElement =  GetWebElement(itensList, i);
			
			if (webElement != null) {
				act.dragAndDrop(webElement, itensList.get(0)).build().perform();
			}
		}
    	    	 
    	listDiv.click();
	}
	
	private static WebElement GetWebElement(List<WebElement> elements, int elementId) {
		
		for (WebElement webElement : elements) {
			
			String elementText = webElement.getText();
			int startPos = elementText.toLowerCase().indexOf(" ");
			int textLength = elementText.length();
			 
			String resultExtractNumber = elementText.substring(++startPos, textLength);
			 
			if (Integer.parseInt(resultExtractNumber) == elementId) {
				return webElement;
			}
		}
		return null;
	}
}
