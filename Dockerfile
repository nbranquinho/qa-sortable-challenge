FROM node:8.9.3

#It's necessary install java
RUN echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y -t jessie-backports openjdk-8-jdk

#It's necessary install Chrome too


#Prepare the working folder
RUN mkdir -p /usr/app/tests

WORKDIR /usr/app/tests

COPY . .

# Add the jar with all the dependencies
ADD  target/MinderaTestQa.jar /usr/app/tests/MinderaTestQa.jar

#prepare the Parcel project and sart it
RUN npm install & npm start -d

# Command line to execute the test
ENTRYPOINT ["/usr/bin/java", "-cp", "/usr/app/tests/MinderaTestQa.jar", "org.testng.TestNG", "-testclass", "com.nbranquinho.mindera.test.TestingClass"]